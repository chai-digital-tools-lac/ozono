from pprint import pprint
import boto3
# import pandas as pd

session = boto3.session.Session()

s3_client = session.client(
    service_name='s3',
    region_name='us-west-1',
    aws_access_key_id='123',
    aws_secret_access_key='123',
    #endpoint_url='http://15.235.116.50:9878',
    endpoint_url='https://s3.chai-digital-tools-lac.com',
)

print(s3_client)

response = s3_client.create_bucket(Bucket='test')
pprint(response)

response = s3_client.list_buckets()
print('Existing buckets:')
for bucket in response['Buckets']:
   print(f'  {bucket["Name"]}')

s3 = boto3.resource(
    service_name='s3',
    endpoint_url='https://s3.chai-digital-tools-lac.com',
    #endpoint_url='http://0.0.0.0:9878',
    region_name='us-west-1',
    aws_access_key_id='123',
    aws_secret_access_key='123'
)

bucket = s3.Bucket('chaidata')
print(bucket)

response = bucket.upload_file('./tmp2.txt','tmp2.txt')

print(response)
